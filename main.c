#include "crc32c.h"
#include <stdint.h>
#include <stdio.h> /* fprintf(), printf(), stderr */
#include <stdlib.h> /* EXIT_SUCCESS, EXIT_FAILURE */
#include <unistd.h> /* STDIN_FILENO, read(), close() */
#include <fcntl.h> /* open() */
#include <string.h> /* strerror(), strcmp() */
#include <errno.h> /* errno */
#include <stddef.h> /* NULL */

#ifndef MATCRC_BUFSZ
#define MATCRC_BUFSZ (4096 * 4)
#endif

unsigned char buf[MATCRC_BUFSZ];
int opt_simple = 0;
int opt_nosize = 0;
int opt_errstop = 0;
int opt_pad = 0;

char *progname;
const char *usage =
	"usage: %s [-hsne] [<filename> ...]\n"
	"\t-h: print this message\n"
	"\t-s: output only the sum\n"
	"\t-n: do not output file size\n"
	"\t-p: pad the file size so filenames align\n"
	"\t-e: do not process any more files if one fails\n";

static inline int dofd(int fd, char *fn) {
	uint32_t crc = 0xFFFFFFFF;
	uint64_t len = 0;
	ssize_t r;
	while ((r = read(fd, buf, sizeof(buf))) > 0) {
		crc = crc32c(crc, buf, r);
		len += r;
	}
	if (fd != STDIN_FILENO)
		close(fd);
	if (r < 0) {
		if (fn)
			fprintf(stderr, "error reading file '%s': %s\n", fn, strerror(errno));
		else fprintf(stderr, "error reading STDIN: %s\n", strerror(errno));
		return -1;
	}
	crc ^= 0xFFFFFFFF;
	if (opt_simple)
		printf("%08X\n", crc);
	else if (opt_nosize)
		printf("%08X %s\n", crc, fn ? fn : "<STDIN>");
	else if (opt_pad)
		printf("%08X %14llu %s\n", crc, (long long unsigned) len, fn ? fn : "<STDIN>");
	else printf("%08X %llu %s\n", crc, (long long unsigned) len, fn ? fn : "<STDIN>");
	return 0;
}

int main(int argc, char *argv[]) {
	int i, fd, nfiles = 0, endargs = 0, ret = EXIT_SUCCESS;
	progname = argv[0];
	for (i = 1; i < argc; ++i) {
		char *arg = argv[i];
		if (*arg++ != '-' || *arg == 0) {
			++nfiles;
			continue;
		}
		if (arg[0] == '-' && arg[1] == 0) {
			nfiles += argc - (i + 1);
			break;
		}
		while (1) {
			switch(*arg) {
				case 's':
					opt_simple = 1;
					break;
				case 'n':
					opt_nosize = 1;
					break;
				case 'e':
					opt_errstop = 1;
					break;
				case 'p':
					opt_pad = 1;
					break;
				case 'h':
					fprintf(stderr, usage, argv[0]);
					return EXIT_SUCCESS;
				case 0:
					goto nextarg;
				default:
					fprintf(stderr, usage, argv[0]);
					return EXIT_FAILURE;
			}
			++arg;
		}
		nextarg:;
	}
	for (i = 1; i < argc; ++i) {
		if (!endargs) {
			if (strcmp(argv[i], "--") == 0) {
				endargs = 1;
				continue;
			}
			if (argv[i][0] == '-') {
				if (argv[i][1] == 0) {
					fd = STDIN_FILENO;
					goto gotstdin;
				}
				continue;
			}
		}
		fd = open(argv[i], O_RDONLY);
		if (fd < 0) {
			fprintf(stderr, "error opening file '%s': %s\n", argv[i], strerror(errno));
			if (opt_errstop)
				return EXIT_FAILURE;
			ret = EXIT_FAILURE;
			continue;
		}
		gotstdin:
		if (dofd(fd, (fd == STDIN_FILENO) ? NULL : argv[i]) < 0) {
			if (opt_errstop)
				return EXIT_FAILURE;
			ret = EXIT_FAILURE;
			continue;
		}
		close(fd);
	}
	if (!nfiles && dofd(STDIN_FILENO, NULL) < 0)
		return EXIT_FAILURE;
	return ret;
}
