#include "tables.h"
#include <stdint.h>
#include <stddef.h> /* size_t */

/* If you decide to read this, I will not be held responsible for resulting eye injuries! */
#ifdef MATCRC_DYNTABLES
	#include <string.h> /* memcpy() */

	static int crc32c_havetables = 0;

	static void crc32c_gentables(void) {
		int i, n;
		crc32c_havetables = 1;
		/* Create tbl0. */
		for (i = 0; i < 256; ++i) {
			crc32c_tbl[0][i] = i;
			for (n = 0; n < 8; ++n)
				crc32c_tbl[0][i] = (crc32c_tbl[0][i] >> 1) ^ (-(crc32c_tbl[0][i] & 1) & 0x82F63B78);
		}
		#ifndef MATCRC_ONLY_HW
			/* Create tbl1-15. */
			for (n = 1; n < 16; ++n) {
				memcpy(crc32c_tbl[n], crc32c_tbl[n - 1], sizeof(crc32c_tbl[n]));
				for (i = 0; i < 256; ++i)
					crc32c_tbl[n][i] = crc32c_tbl[0][crc32c_tbl[n][i] & 0xFF] ^ (crc32c_tbl[n][i] >> 8);
			}
		#endif
		/* Some ugly macros. */
		#define step(x) (crc32c_tbl[0][x & 0xFF] ^ (x >> 8))
		#define quadstep(x, tbl)\
			(tbl[(x >> 24) & 0xFF] ^\
			 step(tbl[(x >> 16) & 0xFF]) ^\
			 step(step(tbl[(x >> 8) & 0xFF])) ^\
			 step(step(step(tbl[(x >> 0) & 0xFF]))))
		#ifndef MATCRC_NO_COMBINE
			/* Create the tables for crc32c_combine(). */
			#ifdef MATCRC_ONLY_HW /* We want one extra table if there's no tables 1-15. */
				memcpy(crc32c_tbl_combine[0], crc32c_tbl[0], sizeof(crc32c_tbl[0]));
				for (n = 0; n < 12; ++n) for (i = 0; i < 256; ++i)
					crc32c_tbl_combine[0][i] = crc32c_tbl[0][crc32c_tbl_combine[0][i] & 0xFF] ^ (crc32c_tbl_combine[0][i] >> 8);
				for (n = 1; n < 60; ++n) for (i = 0; i < 256; ++i) {
					const uint32_t ca = crc32c_tbl_combine[n - 1][i];
					crc32c_tbl_combine[n][i] = quadstep(ca, crc32c_tbl_combine[n - 1]);
				}
			#else /* And if we do have tables 1-15, we can use them to get going faster. */
				for (i = 0; i < 256; ++i) {
					const uint32_t ca = crc32c_tbl[12][i];
					crc32c_tbl_combine[0][i] = crc32c_tbl[12][(ca >> 24) & 0xFF] ^ crc32c_tbl[13][(ca >> 16) & 0xFF] ^ crc32c_tbl[14][(ca >> 8) & 0xFF] ^ crc32c_tbl[15][(ca >> 0) & 0xFF];
				}
				for (n = 1; n < 59; ++n) for (i = 0; i < 256; ++i) {
					const uint32_t ca = crc32c_tbl_combine[n - 1][i];
					crc32c_tbl_combine[n][i] = quadstep(ca, crc32c_tbl_combine[n - 1]);
				}
			#endif
		#endif
	}
#endif

#if (defined(MATCRC_SSE64) || defined(MATCRC_SSE32)) && !defined(MATCRC_ONLY_HW)
	#define MATCRC_HAVE_HW /* This means we have optional HW support, not set with MATCRC_ONLY_HW. */

	static int crc32c_hwenabled = -1;

	static void crc32c_hwinit(void) {
		uint32_t ecx;
		__asm__("cpuid" : "=c"(ecx) : "a"(0x01) : "ebx", "edx");
		crc32c_hwenabled = (ecx >> 20) & 1;
	}

	static uint32_t crc32c_sw(uint32_t crc, const void *data, size_t len);
#elif defined(MATCRC_AARCH64) || defined(MATCRC_AARCH32) && !defined(MATCRC_ONLY_HW)
	#if defined(MATCRC_AARCH_MRS)
		#define MATCRC_HAVE_HW

		static int crc32c_hwenabled = -1;

		static void crc32c_hwinit(void) {
			#ifdef MATCRC_AARCH64
			uint64_t isar;
			__asm__("mrs %0, ID_AA64ISAR0_EL1" : "=r"(isar));
			#else
			uint32_t isar;
			__asm__("mrs %0, ID_ISAR5_EL1" : "=r"(isar));
			#endif
			crc32c_hwenabled = (((isar >> 16) & 0xFF) == 1);
		}

		static uint32_t crc32c_sw(uint32_t crc, const void *data, size_t len);
	#elif defined(MATCRC_AARCH_EXT)
		#define MATCRC_HAVE_HW

		static int crc32c_hwenabled = -1;
		extern int crc32c_aarch_hascrcinstr(void);

		static void crc32c_hwinit(void) {
			crc32c_hwenabled = crc32c_aarch_hascrcinstr();
		}

		static uint32_t crc32c_sw(uint32_t crc, const void *data, size_t len);
	#endif
#endif

/* crc32c_init
 * 
 * Do any initialization that'd otherwise have to happen on the first
 * call to crc32c() or crc32c_combine(). Calling this is optional.
 */
void crc32c_init(void) {
	#if defined(MATCRC_HAVE_HW) && !defined(MATCRC_ONLY_HW)
		if (crc32c_hwenabled < 0)
			crc32c_hwinit();
	#endif
	#ifdef MATCRC_DYNTABLES
		#if defined(MATCRC_NO_COMBINE) && defined(MATCRC_HAVE_HW)
			if (!crc32c_havetables && !crc32c_hwenabled)
				crc32c_gentables();
		#else
			if (!crc32c_havetables)
				crc32c_gentables();
		#endif
	#endif
}

/* crc32c (SSE version)
 *
 * Calculate CRC32C using SSE4.2 instruction for it.
 */
#if defined(MATCRC_SSE64) || defined(MATCRC_SSE32)
uint32_t crc32c(uint32_t crc, const void *data, size_t len) {
	#ifndef MATCRC_ONLY_HW
	if (crc32c_hwenabled < 0)
		crc32c_hwinit();
	if (!crc32c_hwenabled)
		return crc32c_sw(crc, data, len);
	#endif
	uint8_t *ptr = (uint8_t *) data, *end = ptr + len;
	#ifdef MATCRC_ALIGN
	for (; (uintptr_t) ptr & 7 && ptr < end; ++ptr)
		__asm__("crc32b (%1), %k0" : "+r"(crc) : "r"(ptr));
	#endif
	#ifdef MATCRC_SSE64
	for (; ptr <= end - 8; ptr += 8)
		__asm__("crc32q (%1), %q0" : "+r"(crc) : "r"(ptr));
	#endif
	for (; ptr <= end - 4; ptr += 4)
		__asm__("crc32l (%1), %k0" : "+r"(crc) : "r"(ptr));
	for (; ptr <= end - 2; ptr += 2)
		__asm__("crc32w (%1), %k0" : "+r"(crc) : "r"(ptr));
	for (; ptr < end; ++ptr)
		__asm__("crc32b (%1), %k0" : "+r"(crc) : "r"(ptr));
	return crc;
}
#endif

/* crc32c (ARMv8-A version)
 *
 * Calculate CRC32C using SSE4.2 instruction for it.
 */
#if defined(MATCRC_AARCH64) || defined(MATCRC_AARCH32)
uint32_t crc32c(uint32_t crc, const void *data, size_t len) {
	#ifndef MATCRC_ONLY_HW
	if (crc32c_hwenabled < 0)
		crc32c_hwinit();
	if (!crc32c_hwenabled)
		return crc32c_sw(crc, data, len);
	#endif
	uint8_t *ptr = (uint8_t *) data, *end = ptr + len;
	#ifdef MATCRC_ALIGN
	for (; ptr < end; ++ptr)
		__asm__("crc32cb %w0, %w0, %w1" : "+r"(crc) : "r"(*((uint8_t *) ptr)));
	#endif
	#ifdef MATCRC_AARCH64
	for (; ptr <= end - 8; ptr += 8)
		__asm__("crc32cx %w0, %w0, %x1" : "+r"(crc) : "r"(*((uint64_t *) ptr)));
	#endif
	for (; ptr <= end - 4; ptr += 4)
		__asm__("crc32cw %w0, %w0, %w1" : "+r"(crc) : "r"(*((uint32_t *) ptr)));
	for (; ptr <= end - 2; ptr += 2)
		__asm__("crc32ch %w0, %w0, %w1" : "+r"(crc) : "r"(*((uint16_t *) ptr)));
	for (; ptr < end; ++ptr)
		__asm__("crc32cb %w0, %w0, %w1" : "+r"(crc) : "r"(*((uint8_t *) ptr)));
	return crc;
}
#endif

/* crc32c
 *
 * Calculate CRC32C using slice-by-16 (and 8 and 4) algorithm.
 * Start with a crc value of 0xFFFFFFFF and on finishing xor with
 *   0xFFFFFFFF to get the correct sum.
 */
#ifndef MATCRC_ONLY_HW
#ifdef MATCRC_HAVE_HW
static uint32_t crc32c_sw(uint32_t crc, const void *data, size_t len) {
#else
uint32_t crc32c(uint32_t crc, const void *data, size_t len) {
#endif
	uint8_t *ptr = (uint8_t *) data, *end = ptr + len;
	#ifdef MATCRC_DYNTABLES
	if (!crc32c_havetables)
		crc32c_gentables();
	#endif
	#ifdef MATCRC_ALIGN
	/* Align the pointer before processing bigger chunks. */
	while ((uintptr_t) ptr & 7 && ptr < end)
		crc = crc32c_tbl[0][(crc ^ *ptr++) & 0xFF] ^ (crc >> 8);
	#endif
	/* Slice-by-16. */
	while (ptr <= end - 16) {
		/* On amd64 this is helps a lot but on aarch64 and qemu-ppc only a bit. */
		crc = crc32c_tbl[0][ptr[15]] ^ crc32c_tbl[1][ptr[14]] ^
		      crc32c_tbl[2][ptr[13]] ^ crc32c_tbl[3][ptr[12]] ^
		      crc32c_tbl[4][ptr[11]] ^ crc32c_tbl[5][ptr[10]] ^
		      crc32c_tbl[6][ptr[9]] ^ crc32c_tbl[7][ptr[8]] ^
		      crc32c_tbl[8][ptr[7]] ^ crc32c_tbl[9][ptr[6]] ^
		      crc32c_tbl[10][ptr[5]] ^ crc32c_tbl[11][ptr[4]] ^
		      crc32c_tbl[12][ptr[3] ^ ((crc >> 24) & 0xFF)] ^ crc32c_tbl[13][ptr[2] ^ ((crc >> 16) & 0xFF)] ^
		      crc32c_tbl[14][ptr[1] ^ ((crc >>  8) & 0xFF)] ^ crc32c_tbl[15][ptr[0] ^ ((crc >>  0) & 0xFF)];
		ptr += 16;
	}
	/* Slice-by-8. */
	while (ptr <= end - 8) {
		crc = crc32c_tbl[0][ptr[7]] ^ crc32c_tbl[1][ptr[6]] ^
		      crc32c_tbl[2][ptr[5]] ^ crc32c_tbl[3][ptr[4]] ^
		      crc32c_tbl[4][ptr[3] ^ ((crc >> 24) & 0xFF)] ^ crc32c_tbl[5][ptr[2] ^ ((crc >> 16) & 0xFF)] ^
		      crc32c_tbl[6][ptr[1] ^ ((crc >>  8) & 0xFF)] ^ crc32c_tbl[7][ptr[0] ^ ((crc >>  0) & 0xFF)];
		ptr += 8;
	}
	/* Slice-by-4. */
	while (ptr <= end - 4) {
		crc = crc32c_tbl[0][ptr[3] ^ ((crc >> 24) & 0xFF)] ^ crc32c_tbl[1][ptr[2] ^ ((crc >> 16) & 0xFF)] ^
		      crc32c_tbl[2][ptr[1] ^ ((crc >>  8) & 0xFF)] ^ crc32c_tbl[3][ptr[0] ^ ((crc >>  0) & 0xFF)];
		ptr += 4;
	}
	/* Deal with remaining bytes. */
	while (ptr < end)
		crc = crc32c_tbl[0][(crc ^ *ptr++) & 0xFF] ^ (crc >> 8);
	return crc;
}
#endif

/* crc32c_combine
 *
 * Combine 2 CRC32C sums to get the sum of the concatenated inputs for
 *   said sums. The length of the data the second sum was made from has
 *   to be given for this to work. This works by first appending len1
 *   zeros to sum1 (as if continueing to sum data but only zeros).
 * This function works very fast because the slice-by-n optimalization
 *   can be simplified for it so we need only the last 4 of the tables
 *   for a given n. To understand how this works consider the above
 *   crc32c function with zero value for all slices and the fact that
 *   all the tables start with 0. We use several tables to get an almost
 *   constant execution time regardless of the len1 value.
 */
#ifndef MATCRC_NO_COMBINE
uint32_t crc32c_combine(uint32_t crc0, uint32_t crc1, uint64_t len1) {
	int bit;
	#ifdef MATCRC_DYNTABLES
	if (!crc32c_havetables)
		crc32c_gentables();
	#endif
	/* Some ugly macros. */
	#define step(x) (crc32c_tbl[0][x & 0xFF] ^ (x >> 8))
	#define quadstep(x, tbl)\
		(tbl[(x >> 24) & 0xFF] ^\
		 step(tbl[(x >> 16) & 0xFF]) ^\
		 step(step(tbl[(x >> 8) & 0xFF])) ^\
		 step(step(step(tbl[(x >> 0) & 0xFF]))))
	#ifdef MATCRC_ONLY_HW /* Save some more room on tables, performance impact is very minimal. */
		/* Handle bit 4-63, we have one more table with MATCRC_ONLY_HW. */
		for (bit = 4; bit < 64; ++bit) {
			if (len1 & ((uint64_t) 1 << bit))
				crc0 = quadstep(crc0, crc32c_tbl_combine[bit - 4]);
		}
		/* Looking these up straight from tbl0 seems to be the fastest. */
		if (len1 & (1 << 3)) {
			crc0 = crc32c_tbl[0][crc0 & 0xFF] ^ (crc0 >> 8);
			crc0 = crc32c_tbl[0][crc0 & 0xFF] ^ (crc0 >> 8);
			crc0 = crc32c_tbl[0][crc0 & 0xFF] ^ (crc0 >> 8);
			crc0 = crc32c_tbl[0][crc0 & 0xFF] ^ (crc0 >> 8);
			crc0 = crc32c_tbl[0][crc0 & 0xFF] ^ (crc0 >> 8);
			crc0 = crc32c_tbl[0][crc0 & 0xFF] ^ (crc0 >> 8);
			crc0 = crc32c_tbl[0][crc0 & 0xFF] ^ (crc0 >> 8);
			crc0 = crc32c_tbl[0][crc0 & 0xFF] ^ (crc0 >> 8);
		}
		if (len1 & (1 << 2)) {
			crc0 = crc32c_tbl[0][crc0 & 0xFF] ^ (crc0 >> 8);
			crc0 = crc32c_tbl[0][crc0 & 0xFF] ^ (crc0 >> 8);
			crc0 = crc32c_tbl[0][crc0 & 0xFF] ^ (crc0 >> 8);
			crc0 = crc32c_tbl[0][crc0 & 0xFF] ^ (crc0 >> 8);
		}
	#else /* Take advantage of the tables our slice-by-16 uses too. */
		/* Handle bit 5-63. */
		for (bit = 5; bit < 64; ++bit) {
			if (len1 & ((uint64_t) 1 << bit))
				crc0 = quadstep(crc0, crc32c_tbl_combine[bit - 5]);
		}
		/* This is equivalent to slice-by-16 summing of zeros. */
		if (len1 & (1 << 4)) {
			crc0 = crc32c_tbl[12][(crc0 >> 24) & 0xFF] ^ crc32c_tbl[13][(crc0 >> 16) & 0xFF] ^
				   crc32c_tbl[14][(crc0 >>  8) & 0xFF] ^ crc32c_tbl[15][(crc0 >>  0) & 0xFF];
		}
		/* Small performance improvement is still had with slice-by-8. */
		if (len1 & (1 << 3)) {
			crc0 = crc32c_tbl[4][(crc0 >> 24) & 0xFF] ^ crc32c_tbl[5][(crc0 >> 16) & 0xFF] ^
				   crc32c_tbl[6][(crc0 >>  8) & 0xFF] ^ crc32c_tbl[7][(crc0 >>  0) & 0xFF];
		}
		/* Even slice-by-4 still improved speed a tiny bit. */
		if (len1 & (1 << 2)) {
			crc0 = crc32c_tbl[0][(crc0 >> 24) & 0xFF] ^ crc32c_tbl[1][(crc0 >> 16) & 0xFF] ^
				   crc32c_tbl[2][(crc0 >>  8) & 0xFF] ^ crc32c_tbl[3][(crc0 >>  0) & 0xFF];
		}
	#endif
	/* Deal with the remaining bytes. */
	if (len1 & (1 << 1)) {
		crc0 = crc32c_tbl[0][crc0 & 0xFF] ^ (crc0 >> 8);
		crc0 = crc32c_tbl[0][crc0 & 0xFF] ^ (crc0 >> 8);
	}
	if (len1 & (1 << 0))
		crc0 = crc32c_tbl[0][crc0 & 0xFF] ^ (crc0 >> 8);
	return crc0 ^ crc1;
}
#endif
