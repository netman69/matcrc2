#ifndef __TABLES_H__
#define __TABLES_H__

#include <stdint.h>

/* This is why the file is included in tables.h and crc32c.c */
#if defined(MATCRC_ONLY_HW) && (!defined(MATCRC_SSE64) && !defined(MATCRC_SSE32) && !defined(MATCRC_AARCH64) && !defined(MATCRC_AARCH32))
#undef MATCRC_ONLY_HW
#endif
#if (defined(MATCRC_AARCH64) || defined(MATCRC_AARCH32)) && !defined(MATCRC_AARCH_MRS) && !defined(MATCRC_AARCH_EXT)
#define MATCRC_ONLY_HW
#endif

/* For slice-by-16 algorithm. */
extern uint32_t crc32c_tbl[][256];

/* For faster checksum combining. */
extern uint32_t crc32c_tbl_combine[][256];

#endif /* __TABLES_H__ */
