#ifndef __CRC32C_H__
#define __CRC32C_H__

#include <stdint.h>
#include <stddef.h> /* size_t */

extern void crc32c_init(void);
extern uint32_t crc32c(uint32_t crc, const void *data, size_t len);
extern uint32_t crc32c_combine(uint32_t crc0, uint32_t crc1, uint64_t len1);

#endif /* __CRC32C_H__ */
