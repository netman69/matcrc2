#!/bin/sh

if [ "$#" -ne 2 ]; then
	>&2 echo "Run 'make config' instead."
	exit 1
fi

CC="$1"
CFLAGS="$2"

OPT_SSE="no"
OPT_ARM="no"
OPT_MRS="no"
CFLAGS_PR=""

#TMPFILE="/tmp/$0$$matcrccfg"
TMPFILE="./conftmp"

trim() {
	echo "$1" | awk '{$1=$1};1'
}

# Check if we can even proceed.
if ! (echo 'int main(void) { return 0; }' | $CC -o "$TMPFILE" $CFLAGS -xc - && $TMPFILE) 2> /dev/null; then
	rm -f "$TMPFILE"
	>&2 echo "error: Compiler test failed."
	exit 1
fi
if ! (echo | xargs) 2> /dev/null > /dev/null; then
	>&2 echo "error: The configure script needs xargs."
	exit 1
fi

# Check if SSE4.2 is supported.
# We use 'sh -c "$TMPFILE"' because some errors are otherwise not redirected.
if (echo 'int main(void) { int ecx; __asm__("cpuid" : "=c"(ecx) : "a"(0x01) : "ebx", "edx"); return !((ecx >> 20) & 1); }' | $CC -o "$TMPFILE" $CFLAGS -xc - && sh -c "$TMPFILE") 2> /dev/null; then
	if (echo 'int main(void) { int ecx; __asm__("cpuid" : "=c"(ecx) : "a"(0x01) : "ebx", "edx"); return !((ecx >> 20) & 1); }' | $CC -o "$TMPFILE" $CFLAGS -xc - && sh -c "$TMPFILE") 2> /dev/null; then
		OPT_SSE="64bit"
		CFLAGS_PR=`trim "$CFLAGS_PR -DMATCRC_SSE64"`
	elif (echo 'int main(void) { int ecx; __asm__("cpuid" : "=c"(ecx) : "a"(0x01) : "ebx", "edx"); return !((ecx >> 20) & 1); }' | $CC -o "$TMPFILE" $CFLAGS -xc - && sh -c "$TMPFILE") 2> /dev/null; then
		OPT_SSE="32bit"
		CFLAGS_PR=`trim "$CFLAGS_PR -DMATCRC_SSE32"`
	fi
fi

# Check if ARM crc32c instruction works.
if (echo 'int main(void) { int crc = 0; __asm__("crc32cx %w0, %w0, %x1" : "+r"(crc) : "r"(crc)); return crc; }' | $CC -o "$TMPFILE" $CFLAGS -xc - && sh -c "$TMPFILE") 2> /dev/null; then
	OPT_ARM="64bit"
	CFLAGS_PR=`trim "$CFLAGS_PR -DMATCRC_AARCH64"`
elif (echo 'int main(void) { int crc = 0; __asm__("crc32cx %w0, %w0, %x1" : "+r"(crc) : "r"(crc)); return crc; }' | $CC -o "$TMPFILE" -march=armv8-a+crc $CFLAGS -xc - && sh -c "$TMPFILE") 2> /dev/null; then
	OPT_ARM="64bit"
	CFLAGS_PR=`trim "$CFLAGS_PR -DMATCRC_AARCH64 -march=armv8-a+crc"`
elif (echo 'int main(void) { int crc = 0; __asm__("crc32cw %w0, %w0, %w1" : "+r"(crc) : "r"(crc)); return crc; }' | $CC -o "$TMPFILE" $CFLAGS -xc - && sh -c "$TMPFILE") 2> /dev/null; then
	OPT_ARM="32bit"
	CFLAGS_PR=`trim "$CFLAGS_PR -DMATCRC_AARCH32"`
elif (echo 'int main(void) { int crc = 0; __asm__("crc32cw %w0, %w0, %w1" : "+r"(crc) : "r"(crc)); return crc; }' | $CC -o "$TMPFILE" -march=armv8-a+crc $CFLAGS -xc - && sh -c "$TMPFILE") 2> /dev/null; then
	OPT_ARM="32bit"
	CFLAGS_PR=`trim "$CFLAGS_PR -DMATCRC_AARCH32 -march=armv8-a+crc"`
fi

# Check if we can use MRS instruction.
if (echo 'int main(void) { int isar; __asm__ __volatile__ ("mrs %0, ID_ISAR5_EL1" : "=r"(isar)); return 0; }' | $CC -o "$TMPFILE" $CFLAGS -xc - && sh -c "$TMPFILE") 2> /dev/null; then
	OPT_MRS="yes"
	CFLAGS_PR=`trim "$CFLAGS_PR -DMATCRC_AARCH_MRS -march=armv8-a+crc"`
fi

rm -f "$TMPFILE"

# Show what did we find.
>&2 echo "SSE4.2 CRC32 instruction support: $OPT_SSE"
>&2 echo "ARM CRC32 extension support: $OPT_ARM"
>&2 echo "ARM MRS instruction support: $OPT_MRS"
>&2 echo "Proposed CFLAGS: '$CFLAGS_PR'"
echo "$CFLAGS_PR" > config
