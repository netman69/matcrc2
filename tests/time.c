#include <sys/time.h> /* gettimeofday() */
#include <stdio.h> /* printf(), fprintf(), stderr */
#include <stdint.h> /* uint64_t */
#include <stdlib.h> /* strtoull() */
#include <limits.h> /* strtoull() */

/* Print time in usec since the epoch.
 * If argument given print difference between given time and current.
 */
int main(int argc, char *argv[]) {
	uint64_t t;
	struct timeval time;
	gettimeofday(&time, NULL);
	t = (uint64_t) time.tv_sec * 1000000 + time.tv_usec;
	/* Subtract time if wanted. */
	if (argc > 2) {
		fprintf(stderr, "usage: %s [<previous_time>]\n", argv[0]);
		return EXIT_FAILURE;
	}
	if (argc == 2)
		t = t - strtoull(argv[1], NULL, 10);
	/* Print le result. */
	printf("%llu\n", (long long unsigned) t);
	return 0;
}
