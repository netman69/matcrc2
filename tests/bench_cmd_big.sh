#!/bin/sh

. ../seq.sh

echo "# DataSize Time Speed Sum"
for n in $(seq 100000 100000 10000000); do
	echo -n "$n "
	t=$(./time)
	sum=""
	for i in $(seq 0 100); do
		sum=$(head -c $n /dev/zero | ../crc32c -s)
	done
	t=$(./time "$t")
	awk -v n=$n -v t=$t 'BEGIN { ORS=" "; t /= 1000000; print t, ((t == 0) ? 0 : (n * 1000 / t)) }'
	echo "$sum"
done
