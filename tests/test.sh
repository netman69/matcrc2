#!/bin/sh

. ../seq.sh

fail="no"

# for i in $(seq 0 1024); do ./prng $i 0 $i | ../crc32c -s; done > test1.txt
echo "Testing hash correctness..."
if for i in $(seq 0 1024); do ./prng $i 0 $i | ../crc32c -s; done | diff test1.txt -; then
	echo " * :-) hash correctness test succeeds!"
else
	echo " * :-@ hash correctness test FAILED!"
	fail="yes"
fi

# for i in $(seq 0 128); do ./prng $((1024*1024+i)) 0 $((i+1024)) | ../crc32c -s; done > test2.txt
echo "Testing hash correctness for bigger inputs..."
if for i in $(seq 0 128); do ./prng $((1024*1024+i)) 0 $((i+1024)) | ../crc32c -s; done | diff test2.txt -; then
	echo " * :-) big input hash correctness test succeeds!"
else
	echo " * :-@ big input hash correctness test FAILED!"
	fail="yes"
fi

echo "Testing hash combine correctness..."
if ../crc32c_combine 0 0 0 2> /dev/null > /dev/null; then
	# for i in $(seq 0 1024); do echo -n "`./prng 123 0 $i | ../crc32c -s` "; echo -n "`./prng $i 123 $i | ../crc32c -s` "; echo $i; done > test3_args.txt
	# for i in $(seq 0 1024); do ./prng $((123+i)) 0 $i | ../crc32c -s; done > test3.txt
	if for i in $(seq 0 1024); do awk 'NR=='$((i+1)) test3_args.txt | xargs ../crc32c_combine; done | diff test3.txt -; then
		echo " * :-) hash combine correctness test succeeds!"
	else
		echo " * :-@ hash combine correctness test FAILED!"
		fail="yes"
	fi

	echo "Testing hash combine correctness with bigger inputs..."
	# for i in $(seq 0 1024); do echo -n "`./prng 2048 0 $((i+1024)) | ../crc32c -s` "; echo -n "`./prng $((i*4096)) 2048 $((i+1024)) | ../crc32c -s` "; echo $((i*4096)); done > test4_args.txt
	# for i in $(seq 0 1024); do ./prng $((2048+i*4096)) 0 $((i+1024)) | ../crc32c -s; done > test4.txt
	if for i in $(seq 0 1024); do awk 'NR=='$((i+1)) test4_args.txt | xargs ../crc32c_combine; done | diff test4.txt -; then
		echo " * :-) big input hash combine correctness test succeeds!"
	else
		echo " * :-@ big input hash combine correctness test FAILED!"
		fail="yes"
	fi
else
	echo " * :-] crc32c_combine returns failure, assuming it was disabled at compile time."
fi

if [ "$fail" != "no" ]; then
	echo "One or more of the tests failed."
	exit 1
else
	echo "All tests successful."
fi
