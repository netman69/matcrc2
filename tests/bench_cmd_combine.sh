#!/bin/sh

. ../seq.sh

echo "# DataSize Time Speed Sum"
for n in $(seq 0 1 128); do
	echo -n "$n "
	t=$(./time)
	sum=""
	for i in $(seq 0 1000); do
		sum=$(../crc32c_combine 1065B064 DDFA00B7 $n)
	done
	t=$(./time "$t")
	awk -v n=$n -v t=$t 'BEGIN { ORS=" "; t /= 1000000; print t, ((t == 0) ? 0 : (n * 1000 / t)) }'
	echo "$sum"
done
