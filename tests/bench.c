#include "crc32c.h"
#include <sys/time.h>
#include <stdio.h>
#include <stdint.h>
#include <stddef.h> /* size_t */
#include <stdlib.h> /* strtoul() */
#include <limits.h> /* strtoul() */

unsigned char buf[4096 * 4 + 16];

static inline double tdiff(struct timeval *begin, struct timeval *end) {
	return (end->tv_sec - begin->tv_sec) + (end->tv_usec - begin->tv_usec) / 1000000.0f;
}

int main(int argc, char *argv[]) {
	/* Test performance of crc32c(). */
	size_t i, r;
	uint32_t crc;
	unsigned char *data = buf;
	/* Intentionally misalign data by n bytes. */
	if (argc >= 2)
		data += strtoul(argv[1], NULL, 10) & (16 - 1);
	/* Fill buf with some numbers. */
	for (i = 0; i < sizeof(buf) - 16; ++i)
		data[i] = i;
	/* Collect performance info and print as CSV. */
	printf("# DataSize Time Speed Sum\n");
	for (i = 0; i < sizeof(buf) - 16; ++i) {
		struct timeval begin, end;
		gettimeofday(&begin, NULL);
		for (r = 0; r < 1000; ++r)
			crc = crc32c(0, data, i);
		gettimeofday(&end, NULL);
		double t = tdiff(&begin, &end);
		printf("%lu %f %f %08X\n", (unsigned long) i, t, (t == 0) ? 0 : (((double) 1000 * i) / t), crc);
	}
	return 0;
}
