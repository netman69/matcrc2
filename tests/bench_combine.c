#ifndef MATCRC_NO_COMBINE
#include "crc32c.h"
#include <sys/time.h>
#include <stdio.h>
#include <stdint.h>
#include <stddef.h> /* size_t */
#include <stdlib.h> /* strtoull() */
#include <limits.h> /* strtoull() */

static inline double tdiff(struct timeval *begin, struct timeval *end) {
	return (end->tv_sec - begin->tv_sec) + (end->tv_usec - begin->tv_usec) / 1000000.0f;
}

int main(int argc, char *argv[]) {
	/* Output performance info for crc32c_combine(). */
	uint64_t i, r, o = 0;
	uint32_t crc;
	if (argc >= 2) /* Add value to the size when requested. */
		o = strtoull(argv[1], NULL, 10);
	printf("# DataSize Time Speed Sum\n");
	for (i = 0; i < 4096 * 4; ++i) {
		struct timeval begin, end;
		gettimeofday(&begin, NULL);
		for (r = 0; r < 1000; ++r)
			crc = crc32c_combine(0x1065B064, 0xDDFA00B7, i + o);
		gettimeofday(&end, NULL);
		double t = tdiff(&begin, &end);
		printf("%llu %f %f %08X\n", (unsigned long long) i, t, (t == 0) ? 0 : (((double) 1000 * i) / t), crc);
	}
	return 0;
}
#else
#include <stdio.h> /* fprintf(), stderr */
#include <stdlib.h> /* EXIT_FAILURE */

int main(int argc, char *argv[]) {
	fprintf(stderr, "error: crc32c_combine() was disabled at compile time\n");
	return EXIT_FAILURE;
}
#endif
