PREFIX = /usr/local
CFLAGS += -O3 -std=c99 -Wall -pedantic -Ilib `if [ -e config ]; then cat config; fi`
CFLAGS_LIB = $(CFLAGS) -fpic
LDFLAGS_LIB = $(LDFLAGS) -shared -nodefaultlibs
OBJS = main.o lib/crc32c.o lib/tables.o
OBJS_COMBINE = main_combine.o lib/crc32c.o lib/tables.o
OBJS_SMALL = main.o lib_small/crc32c.o
OBJS_LIB = lib/crc32c.lib.o lib/tables.lib.o
OBJS_SLIB = lib/crc32c.o lib/tables.o
OBJS_BENCH = tests/bench.o lib/crc32c.o lib/tables.o
OBJS_BENCH_COMBINE = tests/bench_combine.o lib/crc32c.o lib/tables.o
.SUFFIXES: .c .o .lib.o

all: config crc32c crc32c_combine libcrc32c.so libcrc32c.a tests/prng tests/time tests/bench tests/bench_combine

config: configure.sh
	./configure.sh "$(CC)" "$(CFLAGS)"

noconfig:
	rm -f config
	touch config

crc32c: $(OBJS) config
	$(CC) $(LDFLAGS) -o $@ $(OBJS)

crc32c_combine: $(OBJS_COMBINE) config
	$(CC) $(LDFLAGS) -o $@ $(OBJS_COMBINE)

crc32c_small: $(OBJS_SMALL)
	$(CC) $(LDFLAGS) -o $@ $(OBJS_SMALL)

libcrc32c.so: $(OBJS_LIB) config
	$(CC) $(LDFLAGS_LIB) -o $@ $(OBJS_LIB)

libcrc32c.a: $(OBJS_SLIB) config
	$(AR) rcs $@ $(OBJS_SLIB)

test: tests/prng crc32c crc32c_combine
	cd tests; ./test.sh

tests/bench: $(OBJS_BENCH)
	$(CC) $(LDFLAGS) -o $@ $(OBJS_BENCH)

tests/bench_combine: $(OBJS_BENCH_COMBINE)
	$(CC) $(LDFLAGS) -o $@ $(OBJS_BENCH_COMBINE)

lib/tables.c: tablegen mktables.sh
	./mktables.sh > $@

lib/tables.o: lib/tables.c

.c.o: # Needed on OpenBSD because it's default one is different.
	$(CC) $(CFLAGS) -c -o $@ $<

.c.lib.o:
	$(CC) $(CFLAGS_LIB) -c -o $@ $<

clean:
	rm -f config crc32c crc32c_combine crc32c_small libcrc32c.so libcrc32c.a tests/prng tests/time tests/bench tests/bench_combine tablegen lib/tables.c $(OBJS) $(OBJS_COMBINE) $(OBJS_SMALL) $(OBJS_LIB) $(OBJS_SLIB) $(OBJS_BENCH) $(OBJS_BENCH_COMBINE)

install: crc32c crc32c_combine libcrc32c.so libcrc32c.a
	install -d $(PREFIX)/bin/
	install crc32c $(PREFIX)/bin/
	install crc32c_combine $(PREFIX)/bin/
	install -d $(PREFIX)/lib/
	install libcrc32c.so $(PREFIX)/lib/
	install libcrc32c.a $(PREFIX)/lib/
	install -d $(PREFIX)/include/
	install -m 644 lib/crc32c.h $(PREFIX)/include/
	install -d $(PREFIX)/man/man1/
	install doc/crc32c.1 $(PREFIX)/man/man1/
	install doc/crc32c_combine.1 $(PREFIX)/man/man1/
	install -d $(PREFIX)/man/man3/
	install doc/crc32c.3 $(PREFIX)/man/man3/
	install doc/crc32c.3 $(PREFIX)/man/man3/crc32c_combine.3
	install doc/crc32c.3 $(PREFIX)/man/man3/crc32c_init.3

deinstall:
	rm -f $(PREFIX)/bin/crc32c $(PREFIX)/bin/crc32c_combine $(PREFIX)/lib/libcrc32c.so $(PREFIX)/lib/libcrc32c.a $(PREFIX)/man/man1/crc32c.1 $(PREFIX)/man/man1/crc32c_combine.1 $(PREFIX)/man/man3/crc32c.3 $(PREFIX)/man/man3/crc32c_combine.3 $(PREFIX)/man/man3/crc32c_init.3
