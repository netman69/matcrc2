#!/bin/sh

. ./seq.sh

# Check if we can proceed.
if ! (awk "" 2> /dev/null > /dev/null) 2> /dev/null; then
	>&2 echo "error: We need awk to proceed."
	exit 1
fi
if ! (./tablegen 0 2> /dev/null > /dev/null) 2> /dev/null; then
	>&2 echo "error: We need the tablegen binary to be built."
	exit 1
fi

# Proceed.
echo "#include \"tables.h\""
echo "#include <stdint.h>"
echo
echo "#ifdef MATCRC_DYNTABLES"
echo "#ifdef MATCRC_ONLY_HW"
echo "uint32_t crc32c_tbl[1][256];"
echo "#ifndef MATCRC_NO_COMBINE"
echo "uint32_t crc32c_tbl_combine[60][256];"
echo "#endif"
echo "#else"
echo "uint32_t crc32c_tbl[16][256];"
echo "#ifndef MATCRC_NO_COMBINE"
echo "uint32_t crc32c_tbl_combine[59][256];"
echo "#endif"
echo "#endif"
echo "#else"
echo
echo "uint32_t crc32c_tbl[][256] = {"
echo "#if !defined(MATCRC_ONLY_HW) || !defined(MATCRC_NO_COMBINE)"
echo "	{"
./tablegen 0 | awk '{ print "\t"$0 }'
echo "	},"
echo "#endif"

echo "#ifndef MATCRC_ONLY_HW"
for i in $(seq 1 15); do
	echo "	{"
	./tablegen $i | awk '{ print "\t"$0 }'
	echo "	},"
done
echo "#endif"
echo "};"
echo
echo "#ifndef MATCRC_NO_COMBINE"
echo "uint32_t crc32c_tbl_combine[][256] = {"

printsub() {
	echo "	{"
	./tablegen $1 | awk '{ print "\t"$0 }'
	echo "	},"
}

echo "#ifdef MATCRC_ONLY_HW"
printsub 12
echo "#endif"
# Below was generated with:
#   for i in $(seq 5 63); do echo printsub $(((1 << i) - 4)); done
# I don't think that'd work on every shell hence it's put here static.
printsub 28
printsub 60
printsub 124
printsub 252
printsub 508
printsub 1020
printsub 2044
printsub 4092
printsub 8188
printsub 16380
printsub 32764
printsub 65532
printsub 131068
printsub 262140
printsub 524284
printsub 1048572
printsub 2097148
printsub 4194300
printsub 8388604
printsub 16777212
printsub 33554428
printsub 67108860
printsub 134217724
printsub 268435452
printsub 536870908
printsub 1073741820
printsub 2147483644
printsub 4294967292
printsub 8589934588
printsub 17179869180
printsub 34359738364
printsub 68719476732
printsub 137438953468
printsub 274877906940
printsub 549755813884
printsub 1099511627772
printsub 2199023255548
printsub 4398046511100
printsub 8796093022204
printsub 17592186044412
printsub 35184372088828
printsub 70368744177660
printsub 140737488355324
printsub 281474976710652
printsub 562949953421308
printsub 1125899906842620
printsub 2251799813685244
printsub 4503599627370492
printsub 9007199254740988
printsub 18014398509481980
printsub 36028797018963964
printsub 72057594037927932
printsub 144115188075855868
printsub 288230376151711740
printsub 576460752303423484
printsub 1152921504606846972
printsub 2305843009213693948
printsub 4611686018427387900
printsub 9223372036854775804

echo "};"
echo "#endif"
echo "#endif"
