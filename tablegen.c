#include <stdint.h>
#include <stdlib.h> /* EXIT_FAILURE, EXIT_SUCCESS, strtoull() */
#include <limits.h> /* strtoull() */
#include <string.h> /* memcpy() */
#include <stdio.h> /* printf() */

static uint32_t tbl[64][4][256];

/* Welcome to hell! */
int main(int argc, char *argv[]) {
	uint64_t i, n, t, tn;

	/* Get the number. */
	if (argc < 2) {
		fprintf(stderr, "give number\n");
		return EXIT_FAILURE;
	}
	tn = strtoull(argv[1], NULL, 10);

	/* Make checksum table. */
	for (i = 0; i < 256; ++i) {
		tbl[0][0][i] = i;
		for (n = 0; n < 8; ++n)
			tbl[0][0][i] = (tbl[0][0][i] >> 1) ^ (-(tbl[0][0][i] & 1) & 0x82F63B78);
	}

	/* Make tables for 1-3 zeros. */
	for (n = 1; n < 4; ++n) {
		memcpy(tbl[0][n], tbl[0][n - 1], sizeof(tbl[0][n]));
		for (i = 0; i < 256; ++i)
			tbl[0][n][i] = tbl[0][0][tbl[0][n][i] & 0xFF] ^ (tbl[0][n][i] >> 8);
	}

	/* Make tables for 2^3 until 2^64 zeros. */
	for (n = 1; n < 64; ++n) for (t = 0; t < 4; ++t) for (i = 0; i < 256; ++i) {
		const uint32_t ca = tbl[n - 1][t][i];
		tbl[n][t][i] = tbl[n - 1][0][(ca >> 24) & 0xFF] ^ tbl[n - 1][1][(ca >> 16) & 0xFF] ^ tbl[n - 1][2][(ca >> 8) & 0xFF] ^ tbl[n - 1][3][(ca >> 0) & 0xFF];
	}

	/* Compose the requested table. */
	uint32_t tblres[256];
	memcpy(tblres, tbl[0][0], sizeof(tblres));
	for (n = 0; n < 64 - 2; ++n) if (tn & ((uint64_t) 4 << n)) for (i = 0; i < 256; ++i) {
		const uint32_t ca = tblres[i];
		tblres[i] = tbl[n][0][(ca >> 24) & 0xFF] ^ tbl[n][1][(ca >> 16) & 0xFF] ^ tbl[n][2][(ca >> 8) & 0xFF] ^ tbl[n][3][(ca >> 0) & 0xFF];
	}
	for (n = 0, t = tn & 0x03; n < t; ++n) for (i = 0; i < 256; ++i)
		tblres[i] = tbl[0][0][tblres[i] & 0xFF] ^ (tblres[i] >> 8);

	/* Print it out. */
	for (i = 0; i < 256; ++i) {
		if ((i + 1) % 8 == 0)
			printf("0x%08X,\n", tblres[i]);
		else if ((i + 1) % 8 == 1)
			printf("\t0x%08X, ", tblres[i]);
		else printf("0x%08X, ", tblres[i]);
	}

	return EXIT_SUCCESS;
}
