#ifndef MATCRC_NO_COMBINE
#include "crc32c.h"
#include <stdint.h>
#include <stdio.h> /* fprintf(), printf(), stderr */
#include <stdlib.h> /* EXIT_SUCCESS, EXIT_FAILURE, strtoul(), strtoull() */
#include <errno.h> /* errno */
#include <string.h> /* strerror() */

const char *usage = "usage: %s <crc0> <crc1> <crc1 length>\n";

int main(int argc, char *argv[]) {
	uint32_t crc0, crc1;
	uint64_t len1;
	if (argc != 4) {
		fprintf(stderr, usage, argv[0]);
		return EXIT_FAILURE;
	}
	errno = 0;
	crc0 = strtoul(argv[1], NULL, 16);
	if (errno != 0)
		goto econv;
	crc1 = strtoul(argv[2], NULL, 16);
	if (errno != 0)
		goto econv;
	len1 = strtoull(argv[3], NULL, 10);
	if (errno != 0)
		goto econv;
	crc0 = crc32c_combine(crc0, crc1, len1);
	printf("%08X\n", crc0);
	return EXIT_SUCCESS;
econv:
	fprintf(stderr, "error parsing argument: %s\n", strerror(errno));
	return EXIT_FAILURE;
}
#else
#include <stdio.h> /* fprintf(), stderr */
#include <stdlib.h> /* EXIT_FAILURE */

int main(int argc, char *argv[]) {
	fprintf(stderr, "error: crc32c_combine() was disabled at compile time\n");
	return EXIT_FAILURE;
}
#endif
